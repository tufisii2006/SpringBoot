package com.example.demo.repo;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.model.Vehicle;

@Repository
public interface VehicleRepo extends MongoRepository<Vehicle, String> {

	public Vehicle findByFin(String fin);

	public List<Vehicle> findByPrice(double price);

	public List<Vehicle> findByBrand(String brand);

	public List<Vehicle> findByPriceOrBrand(double price, String brand);

	public List<Vehicle> findByFinAndBrand(String fin, String brand);

	public List<Vehicle> findByPriceOrderByBrandAsc(double price);

	public Vehicle findLatestByPrice(double price, Sort sort);

}
