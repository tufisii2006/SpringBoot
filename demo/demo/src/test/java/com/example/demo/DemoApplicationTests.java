package com.example.demo;

import static org.junit.Assert.assertThat;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Vehicle;
import com.example.demo.services.VehicleService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	private VehicleService vehicleService;
	
	@Test
	public void testFindByFin() {
		Vehicle vehicleFound=vehicleService.getOneVehicleByfin("unitTest");
		assert vehicleFound !=null;
		assert vehicleFound.getPrice() == 23;		
	}
	@Test
	public void testDelete() {
		List<Vehicle> allVehicleFirst = new LinkedList<Vehicle>();
		allVehicleFirst=vehicleService.getAllVehicles();
		vehicleService.deleteVehicleByfin("999");
		List<Vehicle> allVehiclesNew = new LinkedList<Vehicle>();
		allVehiclesNew=vehicleService.getAllVehicles();
		assert allVehicleFirst.size() > allVehiclesNew.size();
		assert allVehicleFirst.size() == allVehiclesNew.size() +1;
	}

}
