package com.example.demo;

import java.util.LinkedList;
import java.util.List;


import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Vehicle;
import com.example.demo.repo.VehicleRepo;
import com.example.demo.services.VehicleService;


@RunWith(MockitoJUnitRunner.class)
public class MokitoTest {

	@Mock
	private Vehicle vehicle;
	
	@InjectMocks
	private VehicleService service;
	
	@Mock
	VehicleRepo vehicleRepo;
	
	@Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
    public void testGetAll() {
		Vehicle vehicle = new Vehicle();
		vehicle.setFin("test");
		vehicle.setPrice(2);
		vehicle.setBrand("test");
		when(vehicleRepo.findByFin("test")).thenReturn(vehicle);
		Vehicle actualVehile = service.getOneVehicleByfin("test");
        assertEquals("updatedBrand", actualVehile.getBrand());
    }

	
	
}
