package com.example.demo.rest;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Vehicle;
import com.example.demo.services.VehicleService;


@RestController
@RequestMapping("/vehicle")
public class VehicleRestControler {
	
	@Autowired
	private VehicleService vehicleService;
	
	@GetMapping("/all")
	public List<Vehicle> getAllVehicles(){
		return vehicleService.getAllVehicles();		
	}
	
	@GetMapping("/{fin}")
	public Vehicle getOneVehicleByfin(@PathVariable String fin){
		return vehicleService.getOneVehicleByfin(fin);		
	}
		
	@DeleteMapping("/delete/{fin}")
	public void deleteVehicle(@PathVariable String fin) {
		vehicleService.deleteVehicleByfin(fin);
	}
	
	@GetMapping(path = "/all/{price}/{brand}")
	public List<Vehicle> getAllVehiclesWithGivenPriceAndBrand(@PathVariable int price, @PathVariable String brand) {	
		List<Vehicle> all = new LinkedList<Vehicle>();
		all=vehicleService.findByPriceAndBrand(price, brand);	
		return     all;
	}
	
	@PostMapping(path= "/create/{fin}/{brand}")
	public void insertVehicle( @PathVariable String brand, @PathVariable String fin) {
		Vehicle vehicle= new Vehicle();
		vehicle.setFin(fin);
		vehicle.setBrand(brand);
		vehicleService.insertVehicle(vehicle);
	}

}
