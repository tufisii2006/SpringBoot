package com.example.demo.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;
import com.example.demo.model.Vehicle;


@Repository
public interface VehicleRepo extends MongoRepository<Vehicle, String> {

	public Vehicle findByFin(String fin);
	public List<Vehicle> findByPrice(double price);
	public List<Vehicle> findByBrand(String brand);
	public List<Vehicle> findByPriceAndBrand(double price,String brand);
	
} 
