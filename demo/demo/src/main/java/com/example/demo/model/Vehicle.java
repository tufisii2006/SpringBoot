package com.example.demo.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Vehicle {

	private String fin;
	
	private String brand;
	private double price;
	private Date buildDate;
	private int monthsUntilRevision;

	public Vehicle() {

	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}

	public int getMonthsUntilRevision() {
		return monthsUntilRevision;
	}

	public void setMonthsUntilRevision(int monthsUntilRevision) {
		this.monthsUntilRevision = monthsUntilRevision;
	}

}
