package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.example.demo.model.Vehicle;
import com.example.demo.repo.VehicleRepo;

@Component("vehicleService")
public class VehicleService {

	@Autowired
	private VehicleRepo vehicleRepo;

	public void insertVehicle(final Vehicle vehicle) {
		vehicleRepo.save(vehicle);
	}

	public Vehicle getOneVehicleByfin(String fin) {
		List<Vehicle> allVehicles = new ArrayList<>();
		Vehicle vehicle = vehicleRepo.findByFin(fin);
		vehicle.setBrand("updatedBrand");
		//logica 
		return vehicle;
	}

	public List<Vehicle> getAllVehicles() {
		return vehicleRepo.findAll();
	}

	public void deleteVehicleByfin(String fin) {
		Vehicle vehicle = vehicleRepo.findByFin(fin);
		vehicleRepo.delete(vehicle);
	}
	
	public List<Vehicle> findByPriceAndBrand(double price,String brand){
		return vehicleRepo.findByPriceAndBrand(price, brand);
	}

}
