package com.example.demo.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;

@Document
public class Vehicle implements Serializable {

	private static final long serialVersionUID = -2064318110726314109L;

	@Id
	@NonNull
	private String fin;

	private Person person;

	private String brand;

	private double price;

	private Date buildDate;

	private int monthsUntilRevision;

	public Vehicle() {

	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}

	public int getMonthsUntilRevision() {
		return monthsUntilRevision;
	}

	public void setMonthsUntilRevision(int monthsUntilRevision) {
		this.monthsUntilRevision = monthsUntilRevision;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}


	@Override
	public String toString() {
		return "Vehicle [fin=" + fin + ", person=" + person + ", brand=" + brand + ", price=" + price + ", buildDate="
				+ buildDate + ", monthsUntilRevision=" + monthsUntilRevision + "]";
	}

}
