package com.example.demo.mappers;

import com.example.demo.model.Vehicle;
import com.example.demo.restModel.VehicleRequest;

public class RequestToEntity {
	
	
	public static Vehicle mapRequestVehicleToEntityVehicle(VehicleRequest vehicleRequest) {
		Vehicle vehicle= new Vehicle();
		if (vehicleRequest == null) {
			return vehicle;
		}
		vehicle.setBrand(vehicleRequest.getBrand());
		vehicle.setFin(vehicleRequest.getFin());
		vehicle.setPrice(vehicleRequest.getPrice());
		vehicle.setBuildDate(vehicleRequest.getBuildDate());
		vehicle.setPerson(vehicleRequest.getPerson());
		vehicle.setMonthsUntilRevision(vehicleRequest.getMonthsUntilRevision());
		return vehicle;
	}

}
