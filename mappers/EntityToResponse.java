package com.example.demo.mappers;

import com.example.demo.model.Vehicle;
import com.example.demo.restModel.VehicleResponse;

public class EntityToResponse {

	public static VehicleResponse mapVehicleToVehicleResponse(Vehicle vehicle) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		if (vehicle == null) {
			return vehicleResponse;
		} else {
			vehicleResponse.setBrand(vehicle.getBrand());
			vehicleResponse.setBuildDate(vehicle.getBuildDate());
			vehicleResponse.setFin(vehicle.getFin());
			vehicleResponse.setPerson(vehicle.getPerson());
			vehicleResponse.setPrice(vehicle.getPrice());
			return vehicleResponse;
		}

	}

}
