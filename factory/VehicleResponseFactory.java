package com.example.demo.factory;

import com.example.demo.mappers.EntityToResponse;
import com.example.demo.model.Vehicle;
import com.example.demo.restModel.VehicleResponse;

public class VehicleResponseFactory {


	public static VehicleResponse getVehicleInstance(Vehicle vehicle) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse=EntityToResponse.mapVehicleToVehicleResponse(vehicle);
		return vehicleResponse;
	}
	public static VehicleResponse getVehicleErrorInstance(String errorMessage) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setErrorMessage(errorMessage);
		return vehicleResponse;
	}
	
	public static VehicleResponse getVehicleResponseSuccessMessage(String message) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setErrorMessage(message);
		return vehicleResponse;
	}

}
