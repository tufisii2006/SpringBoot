package com.example.demo.factory;
import com.example.demo.mappers.RequestToEntity;
import com.example.demo.model.Vehicle;
import com.example.demo.restModel.VehicleRequest;

public class VehicleRequestFactory {

	public static Vehicle getVehicleInstance(VehicleRequest vehicleRequest) {
		Vehicle vehicleResponse = new Vehicle();
		vehicleResponse=RequestToEntity.mapRequestVehicleToEntityVehicle(vehicleRequest);
		return vehicleResponse;
	}
}
