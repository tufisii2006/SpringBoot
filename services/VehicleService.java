package com.example.demo.services;

import java.util.List;
import java.util.Optional;
import com.example.demo.model.Vehicle;
import com.example.demo.restModel.VehicleRequest;

public interface VehicleService {

	Optional<Vehicle> findById(String id) throws Exception;

	List<Vehicle> findByPriceOrBrand(double price, String brand) throws Exception;

	List<Vehicle> findByFinAndBrand(String fin, String brand) throws Exception;

	Vehicle findOneVehicleByFin(String fin) throws Exception;

	List<Vehicle> findAllVehicles() throws Exception;

	void deleteVehicleByfin(String fin) throws Exception;

	void insertVehicle(final Vehicle vehicle) throws Exception;

	Vehicle updateVehicleInfo(VehicleRequest newVehicle) throws Exception;

}
