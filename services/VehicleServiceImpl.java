package com.example.demo.services;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import com.example.demo.model.Vehicle;
import com.example.demo.repo.VehicleRepo;
import com.example.demo.restModel.VehicleRequest;

@Component("VehicleService")
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleRepo vehicleRepo;

	public Optional<Vehicle> findById(String id) throws Exception{
		return vehicleRepo.findById(id);		
	}

	public List<Vehicle> findByPriceOrBrand(double price, String brand) throws Exception {
		List<Vehicle> allQueriedVehicles = vehicleRepo.findByPriceOrBrand(price, brand);
		if (CollectionUtils.isEmpty(allQueriedVehicles)) {
			throw new Exception();
		} else {
			return allQueriedVehicles;
		}
	}

	public List<Vehicle> findByFinAndBrand(String fin, String brand) throws Exception {
		List<Vehicle> allQueriedVehicles = vehicleRepo.findByFinAndBrand(fin, brand);
		if (CollectionUtils.isEmpty(allQueriedVehicles)) {
			throw new Exception();
		} else {
			return allQueriedVehicles;
		}
	}

	public Vehicle findOneVehicleByFin(String fin) throws Exception {
		Vehicle vehicle = vehicleRepo.findByFin(fin);
		if (vehicle == null) {
			throw new Exception();
		} else {
			return vehicle;
		}
	}

	public List<Vehicle> findAllVehicles() throws Exception {
		List<Vehicle> allVehiclesFromDB = vehicleRepo.findAll();
		if (CollectionUtils.isEmpty(allVehiclesFromDB)) {
			throw new Exception();
		} else {
			return allVehiclesFromDB;
		}
	}

	public void deleteVehicleByfin(String fin) throws Exception {
		Vehicle vehicle = vehicleRepo.findByFin(fin);
		if (vehicle == null) {
			throw new Exception();
		} else {
			vehicleRepo.delete(vehicle);
		}
	}

	public void insertVehicle(final Vehicle vehicle) throws Exception {
		if (vehicle == null) {
			throw new Exception();
		}
		vehicleRepo.save(vehicle);
	}

	public Vehicle updateVehicleInfo(VehicleRequest newVehicle) throws Exception {
		Vehicle oldVehicle = vehicleRepo.findByFin(newVehicle.getFin());
		if (oldVehicle == null) {
			throw new Exception("No vehicle found");
		}
		oldVehicle.setBrand(newVehicle.getBrand());
		oldVehicle.setMonthsUntilRevision(newVehicle.getMonthsUntilRevision());
		oldVehicle.setPrice(newVehicle.getPrice());
		oldVehicle.setBuildDate(newVehicle.getBuildDate());
		oldVehicle.setFin(newVehicle.getFin());
		vehicleRepo.save(oldVehicle);
		return oldVehicle;
	}
}
