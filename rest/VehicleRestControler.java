package com.example.demo.rest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.factory.VehicleRequestFactory;
import com.example.demo.factory.VehicleResponseFactory;
import com.example.demo.model.Vehicle;
import com.example.demo.restModel.VehicleRequest;
import com.example.demo.restModel.VehicleResponse;
import com.example.demo.services.VehicleService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/vehicle")
@Qualifier("VehicleService")
public class VehicleRestControler {

	@Autowired
	private VehicleService vehicleService;

	@RequestMapping(path = "/all", method = RequestMethod.GET, produces = { "application/xml", "application/json" })
	public ResponseEntity<List<VehicleResponse>> getAllVehicles() {
		List<Vehicle> allVehicles = new ArrayList<Vehicle>();
		List<VehicleResponse> allResponseVehicles = new ArrayList<VehicleResponse>();
		try {
			allVehicles = vehicleService.findAllVehicles();
			for (Vehicle vehicle : allVehicles) {
				VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleInstance(vehicle);
				allResponseVehicles.add(vehicleResponse);
			}
			return new ResponseEntity<List<VehicleResponse>>(allResponseVehicles, HttpStatus.OK);
		} catch (Exception e) {
			VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleErrorInstance(e.getMessage());
			allResponseVehicles.add(vehicleResponse);
			return new ResponseEntity<List<VehicleResponse>>(allResponseVehicles, HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(path = "/{fin}", method = RequestMethod.GET, produces = { "application/xml", "application/json" })
	public ResponseEntity<VehicleResponse> getOneVehicleByfin(@PathVariable String fin) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		try {
			Vehicle vehicle = vehicleService.findOneVehicleByFin(fin);			
			vehicleResponse = VehicleResponseFactory.getVehicleInstance(vehicle);
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.FOUND);
		} catch (Exception e) {
			vehicleResponse = VehicleResponseFactory.getVehicleErrorInstance(e.getMessage());
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.NOT_FOUND);
		}

	}

	@ApiOperation(value = "Delete a vehicle method", hidden = true)
	@RequestMapping(path = "/delete/{fin}", method = RequestMethod.DELETE)
	public ResponseEntity<VehicleResponse> deleteVehicle(@PathVariable String fin) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		try {
			vehicleService.deleteVehicleByfin(fin);
			vehicleResponse = VehicleResponseFactory
					.getVehicleResponseSuccessMessage("The vehicle was succesfull deleted !");
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.OK);
		} catch (Exception e) {
			vehicleResponse = VehicleResponseFactory.getVehicleErrorInstance(e.getMessage());
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.NOT_FOUND);
		}

	}

	// @ApiOperation(value = "Create a vehicle method", hidden = true)
	@RequestMapping(path = "/create/", method = RequestMethod.POST, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<VehicleResponse> insertVehicle(@RequestBody VehicleRequest vehicleRequest) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		try {
			Vehicle vehicle = VehicleRequestFactory.getVehicleInstance(vehicleRequest);
			vehicleService.insertVehicle(vehicle);
			vehicleResponse = VehicleResponseFactory.getVehicleInstance(vehicle);
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.OK);
		} catch (Exception e) {
			vehicleResponse = VehicleResponseFactory.getVehicleErrorInstance(e.getMessage());
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/update/", method = RequestMethod.PUT, consumes = { "application/xml",
			"application/json" })
	public ResponseEntity<VehicleResponse> updateVehicleInfo(@RequestBody VehicleRequest vehicleRequest) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		try {
			Vehicle updatedVehicle = vehicleService.updateVehicleInfo(vehicleRequest);
			vehicleResponse = VehicleResponseFactory.getVehicleInstance(updatedVehicle);
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.OK);
		} catch (Exception e) {
			vehicleResponse = VehicleResponseFactory.getVehicleErrorInstance(e.getMessage());
			return new ResponseEntity<VehicleResponse>(vehicleResponse, HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(path = "/all/{price}/{brand}", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<List<VehicleResponse>> getAllVehiclesWithGivenPriceOrBrand(@PathVariable double price,
			@PathVariable String brand) {
		List<Vehicle> allVehicles = new LinkedList<Vehicle>();
		List<VehicleResponse> allResponseVehicles = new ArrayList<VehicleResponse>();
		try {
			allVehicles = vehicleService.findByPriceOrBrand(price, brand);
			for (Vehicle vehicle : allVehicles) {
				VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleInstance(vehicle);
				allResponseVehicles.add(vehicleResponse);
			}
			return new ResponseEntity<List<VehicleResponse>>(allResponseVehicles, HttpStatus.OK);
		} catch (Exception e) {
			VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleErrorInstance(e.getMessage());
			allResponseVehicles.add(vehicleResponse);
			return new ResponseEntity<List<VehicleResponse>>(allResponseVehicles, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(path = "/all/and/{fin}/{brand}", method = RequestMethod.GET, produces = { "application/xml",
			"application/json" })
	public ResponseEntity<List<VehicleResponse>> getAllVehiclesWithGivenFinAndBrand(@PathVariable String fin,
			@PathVariable String brand) {
		List<VehicleResponse> allResponseVehicles = new ArrayList<VehicleResponse>();
		try {
			List<Vehicle> allDatabaseVehicles = vehicleService.findByFinAndBrand(fin, brand);

			for (Vehicle vehicle : allDatabaseVehicles) {
				VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleInstance(vehicle);
				allResponseVehicles.add(vehicleResponse);
			}
			return new ResponseEntity<List<VehicleResponse>>(allResponseVehicles, HttpStatus.OK);
		} catch (Exception e) {
			VehicleResponse vehicleResponse = VehicleResponseFactory.getVehicleErrorInstance(e.getMessage());
			allResponseVehicles.add(vehicleResponse);
			return new ResponseEntity<List<VehicleResponse>>(allResponseVehicles, HttpStatus.BAD_REQUEST);
		}
	}

}
