package com.example.demo.restModel;

import java.util.Date;
import org.springframework.data.annotation.Id;
import com.example.demo.model.Person;
import io.swagger.annotations.ApiModelProperty;

public class VehicleRequest {
	
	@Id
	@ApiModelProperty(notes = "The fin of the vehicle.Must not be null")
	private String fin;

	@ApiModelProperty(notes = "The owner of the vehicle")
	private Person person;

	@ApiModelProperty(notes = "The brand of the vehicle")
	private String brand;

	@ApiModelProperty(notes = "The price of the vehicle.Must be positive value")
	private double price;

	@ApiModelProperty(notes = "The Date in which the vehicle was created")
	private Date buildDate;

	@ApiModelProperty(notes = "Months remaining until the revision")
	private int monthsUntilRevision;

	private String errorMessage;

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}

	public int getMonthsUntilRevision() {
		return monthsUntilRevision;
	}

	public void setMonthsUntilRevision(int monthsUntilRevision) {
		this.monthsUntilRevision = monthsUntilRevision;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
	

}
